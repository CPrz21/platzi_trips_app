import 'package:flutter/material.dart';
import 'place.dart';
import 'profile_place_info.dart';

class ProfilePlace extends StatelessWidget {

  String pathImage;
  Place place;

  ProfilePlace(this.pathImage, this.place);

  @override
  Widget build(BuildContext context) {

    final photoCard = Container(
      height:220.0,
      margin: EdgeInsets.only(bottom: 70.0, top:10.0),
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
            image: AssetImage(pathImage)
        ),
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          shape: BoxShape.rectangle,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color:Colors.black38,
                blurRadius:  15.0,
                offset:  Offset(0.0, 5.0)
            )
          ]
      ),

    );



    return Stack(
      alignment: Alignment(0.1, 0.6),
      children: <Widget>[
        photoCard,
        ProfilePlaceInfo(place)
      ],
    );
  } 

}