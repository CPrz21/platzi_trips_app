import 'package:flutter/material.dart';

class FloatingActionButtonGreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FloatingActionButtonGreen();
  }

}

class _FloatingActionButtonGreen extends State<FloatingActionButtonGreen> {

  bool favorite = false;

  void onPressedFav(){

    setState(() {
      favorite = !favorite;
    });

    final text = favorite ? "Added" : "Removed";

    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text('$text to your favorites'),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FloatingActionButton(
      backgroundColor: Color(0xFF11DA53),
      mini:true,
      tooltip: "Fav",
      onPressed: onPressedFav,
      child:Icon( favorite ? Icons.favorite : Icons.favorite_border ),
    );
  }

}