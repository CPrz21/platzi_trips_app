import "package:flutter/material.dart";
import "review.dart";

class ReviewList extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Review("assets/img/traveler.jpg", "Aida Flores", "1 review 5 photos", "Awesome place in El Salvador"),
        new Review("assets/img/traveler.jpg", "Carlos Perez", "1 review 2 photos", "Amazing place in El Salvador"),
        new Review("assets/img/traveler.jpg", "Andrea Perez", "1 review 1 photos", "Pretty place in El Salvador")
      ],
    );
  }

}