import 'package:flutter/material.dart';

class CircleButton extends StatefulWidget {

  bool mini;
  var icon;
  double iconSize;
  var color;

  CircleButton(this.mini, this.icon, this.iconSize, this.color);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CircleButton();
  }

}

class _CircleButton extends State<CircleButton>{

  void onPressedButton(){
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text('Touched'),
    ));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FloatingActionButton(
      backgroundColor: widget.color,
      mini: widget.mini,
      onPressed: onPressedButton,
      child: Icon(
        widget.icon,
        size: widget.iconSize,
        color: Color(0xFF4268D3),
      ),
    );
  }

}