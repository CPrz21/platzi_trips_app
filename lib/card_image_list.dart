import 'package:flutter/material.dart';
import 'card_image.dart';

class CardImageList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 350.0,
      child: ListView(
        padding: EdgeInsets.all(25.0),
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          CardImage('assets/img/beach-dawn.jpg'),
          CardImage('assets/img/beach-calm-clouds.jpg'),
          CardImage('assets/img/altitude-clouds-cloudy.jpg'),
          CardImage('assets/img/adventure-altitude-clouds.jpg'),
          CardImage('assets/img/adventure-altitude-backpack.jpg'),
        ],
      ),
    );
  }

}