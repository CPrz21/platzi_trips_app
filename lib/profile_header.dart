import 'package:flutter/material.dart';
import 'user_info.dart';
import 'button_bar.dart';

class ProfileHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final title = Text(
      'Profile',
      style: TextStyle(
          fontFamily: 'Lato',
          fontSize: 30.0,
          color:Colors.white,
          fontWeight: FontWeight.bold
      ),
    );

    return Container(
      margin: EdgeInsets.only(
        top:60.0,
        left: 20.0,
        right:20.0
      ),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              title,
              Icon(Icons.settings, color: Colors.white,),
            ],
          ),
          UserInfo("assets/img/me.jpg", "Carlos Pérez", "faselona@gmail.com"),
          ButtonsBar(),
        ],
      ),
    );
  }

}