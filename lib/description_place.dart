import 'package:flutter/material.dart';
import 'button_purple.dart';

class DescriptionPlace extends StatelessWidget{

  String namePlace;
  int stars;
  String descriptionTextPlace;

  DescriptionPlace(this.namePlace, this.stars, this.descriptionTextPlace);

  String descriptionText='''Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corrupti, iste, voluptatum reprehenderit quisquam ad fugiat, sint quia cumque facilis dolorem minus vero. Atque eius quam tempore, animi modi quisquam commodi! 
    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corrupti, iste, voluptatum reprehenderit quisquam ad fugiat, sint quia cumque facilis dolorem minus vero. Atque eius quam tempore, animi modi quisquam commodi!''';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final star_half = Container(
        margin: EdgeInsets.only(
            top: 363.0,
            right:3.0
        ),
        child: Icon(
            Icons.star_half,
            color: Color(0xFFf2C611)
        )
    );

    final star = Container(
      margin: EdgeInsets.only(
        top: 363.0,
        right:3.0
      ),
      child: Icon(
          Icons.star,
        color: Color(0xFFf2C611)
      )
    );

    final star_border = Container(
        margin: EdgeInsets.only(
            top: 363.0,
            right:3.0
        ),
        child: Icon(
            Icons.star_border,
            color: Color(0xFFf2C611)
        )
    );

    final title_stars = Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top:360.0,
            left:20.0,
            right: 20.0
          ),

          child: Text(
            namePlace,
            style: TextStyle(
              fontFamily: "Lato",
              fontSize: 30.0,
              fontWeight: FontWeight.w900
            ),
//            textAlign: TextAlign.left,
          ),
        ),
        Row(
          children: <Widget>[
            star,
            star,
            star,
            star,
            star_half
          ],
        )
      ],
    );
    
    final text_description = Container(
      margin:EdgeInsets.only(
        top:10.0,
        right:20.0,
        left:20.0
      ),
      child: new Text(descriptionTextPlace,
        style: TextStyle(
          fontFamily: "Lato",
          fontSize: 15.0,
          color: Color(0xFF56575a)
        ),
        textAlign: TextAlign.left,
      ),

    );

    final descriptionPlace = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        title_stars,
        text_description,
        ButtonPurple("Navigate")
      ],
    );


    return descriptionPlace;
  }

}